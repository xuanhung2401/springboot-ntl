# Nói tóm lại (NTL) - Spring Data JPA và Mysql

## Output

Cài đặt và xây dựng một api cơ bản với các thao tác thêm sửa xoá sử dụng Spring Data Jpa và cơ sở dữ liệu MySQL.

## Input

- Kiến thức cơ bản về Spring Boot. 
- Các link bài viết liên quan.
- JDK8+.
- Mysql (XAMPP).

## Về Spring Data JPA

- Java JDBC là một java API được sử dụng để kết nối và thực hiện truy vấn với cơ sở dữ liệu.
- JPA - Java Persistence API, là một đặc tả các tiêu chuẩn của Java để làm việc với cơ sở dữ liệu quan hệ. JPA không chứa bất kỳ phương thức thực thi nào, nó cần một JPA implementation triển khai tất cả các đặc tả đã định nghĩa. Những ORM tools như Hibernate, TopLink đều cung cấp trình triển khai cho JPA.
- ORM viết tắt của object-relational-mapping, công nghệ cho phép chuyển đổi từ các object trong lập trình OOP sang database quan hệ và ngược lại. 
- Hibernate là một trong những ORM tools phổ biến được sử dụng nhiều trong các ứng dụng Java.
- Spring Data JPA.

</details>

## Cài đặt

### Cấu trúc dự án

```
├───main
│   ├───java
│   │   └───springhero
│   │       └───example
│   │           ├───entity
│   │           ├───repository
│   │           ├───restapi
│   │           └───service
│   └───resources
│       ├───static
│       └───templates

```
### Maven Repository

Thêm các dependency vào file **pom.xml**

```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
```
<details>
<summary>Chi tiết về dependency</summary>

- **mysql-connector-java**
- **spring-boot-starter-data-jpa**
- **lombok**
</details>


### Cấu hình kết nối database

Cấu hình kết nối đến database trong mysql trong file **resources/application.properties**.

```properties
spring.datasource.url=jdbc:mysql://localhost:3306/spring_jpa?useSSL=false
spring.datasource.username=root
spring.datasource.password=
spring.jpa.hibernate.ddl-auto=update
spring.jpa.show-sql=true
```

<details>
	<summary>Một số giá trị khác của spring.jpa.hibernate.ddl-auto</summary>
	
- **validate**: kiểm tra sự thay đổi giữa các lớp trong project với các bảng, báo lỗi nếu có sự thay đổi, không thay đổi database.
- **update**: thay đổi database khi có sự thay đổi trong model. Không xoá các bảng cũ kể cả không còn sử dụng.
- **create**: xoá database đang tồn tại, tạo mới lại database.
- **create-drop**: gần giống create nhưng xoá database sau khi thao tác. Áp dụng trong quá trình phát triển, đặc biệt là quát trình test.
- **none**: thực hiện trong môi trường production.
</details>

### Mapping entity

```java
package springhero.example.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "accounts")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String lastName;

    @CreationTimestamp
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;
}

```
<details>
	<summary>Về các annotation</summary>

- **@Entity**
- **@Table**
- **@Id**
- Lombok annotation **@Getter**, **@Setter**, **@NoArgsConstructor**, **@AllArgsConstructor**
- **@CreationTimestamp**
- **@UpdateTimestamp**

</details>

### Tạo repository

```java
package springhero.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springhero.example.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
}
```
<details>
	<summary>Về JpaRepository</summary>

- Generic type ``<Account, Long>``
- Các Repository khác.
</details>

### Service

"The repository is where the data is stored. The service is what manipulates the data"

```java
package springhero.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springhero.example.entity.Account;
import springhero.example.repository.AccountRepository;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public Optional<Account> findById(Long id) {
        return accountRepository.findById(id);
    }

    public Account save(Account account) {
        return accountRepository.save(account);
    }

    public void deleteById(Long id) {
        accountRepository.deleteById(id);
    }
}

```

## Các thao tác CRUD cơ bản

### Lấy danh sách

```java
@RequestMapping(method = RequestMethod.GET)
public ResponseEntity<List<Account>> getList() {
    return ResponseEntity.ok(accountService.findAll());
}
```

### Lấy chi tiết

```java
@RequestMapping(method = RequestMethod.GET, path = "{id}")
public ResponseEntity<?> getDetail(@PathVariable Long id) {
    Optional<Account> optionalAccount = accountService.findById(id);
    if (!optionalAccount.isPresent()) {
        ResponseEntity.badRequest().build();
    }
    return ResponseEntity.ok(optionalAccount.get());
}
```

### Thêm mới

```java
@RequestMapping(method = RequestMethod.POST)
public ResponseEntity<Account> create(@RequestBody Account product) {
    return ResponseEntity.ok(accountService.save(product));
}
```

### Sửa thông tin

```java
@RequestMapping(method = RequestMethod.PUT, path = "{id}")
public ResponseEntity<Account> update(@PathVariable Long id, @RequestBody Account account) {
    Optional<Account> optionalAccount = accountService.findById(id);
    if (!optionalAccount.isPresent()) {
        ResponseEntity.badRequest().build();
    }
    Account existAccount = optionalAccount.get();
    // map object
    existAccount.setFirstName(account.getFirstName());
    existAccount.setLastName(account.getLastName());
    return ResponseEntity.ok(accountService.save(existAccount));
}
```

### Xoá thông tin

```java
@RequestMapping(method = RequestMethod.DELETE, path = "{id}")
public ResponseEntity<?> delete(@PathVariable Long id) {
    if (!accountService.findById(id).isPresent()) {
        ResponseEntity.badRequest().build();
    }
    accountService.deleteById(id);
    return ResponseEntity.ok().build();
}
```
### Phần xử lý api cơ bản

```java
package springhero.example.restapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springhero.example.entity.Account;
import springhero.example.service.AccountService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/accounts")
public class AccountApi {

    @Autowired
    AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Account>> getList() {
        return ResponseEntity.ok(accountService.findAll());
    }

    @RequestMapping(method = RequestMethod.GET, path = "{id}")
    public ResponseEntity<?> getDetail(@PathVariable Long id) {
        Optional<Account> optionalAccount = accountService.findById(id);
        if (!optionalAccount.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(optionalAccount.get());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Account> create(@RequestBody Account product) {
        return ResponseEntity.ok(accountService.save(product));
    }

    @RequestMapping(method = RequestMethod.PUT, path = "{id}")
    public ResponseEntity<Account> update(@PathVariable Long id, @RequestBody Account account) {
        Optional<Account> optionalAccount = accountService.findById(id);
        if (!optionalAccount.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        Account existAccount = optionalAccount.get();
        // map object
        existAccount.setFirstName(account.getFirstName());
        existAccount.setLastName(account.getLastName());
        return ResponseEntity.ok(accountService.save(existAccount));
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        if (!accountService.findById(id).isPresent()) {
            ResponseEntity.badRequest().build();
        }
        accountService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
```

Tham khảo code tại [link git](https://gitlab.com/xuanhung2401/spring-data-jpa-basic)



